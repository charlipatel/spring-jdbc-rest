package com.company.test;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.company.dao.JdbcUserDAO;
import com.company.model.User;

public class UserServiceTest {

	@Autowired
	private JdbcUserDAO userDAO;	
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void insertTest() {
		User user = new User();
		user.setFirstName("test first name");
		user.setFirstName("test first name");		
		userDAO.insert(user);		
	}

	@Test
	public void listTest() {		
		List<User> users =userDAO.list();
		for (User user : users) {
			System.out.println("User ID " + user.getId());
			System.out.println("User First Name " + user.getFirstName());
			System.out.println("User Last Name " + user.getLastName());
		}
	}
}