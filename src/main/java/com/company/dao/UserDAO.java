package com.company.dao;

import java.util.List;

import com.company.model.User;

public interface UserDAO {
   public void insert(User user);
   public List<User> list();
}