package com.company.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.company.model.User;

public class JdbcUserDAO {
		private DataSource dataSource;
		public void setDataSource(DataSource dataSource) {
			this.dataSource = dataSource;
		}
	 
		public void insert(User user){	 
			String sql = "INSERT INTO USER " +
					"(first_name, last_name) VALUES (?, ?)";
			Connection conn = null;
	 
			try {
				conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql);
				ps.setString(1, user.getFirstName());
				ps.setString(2, user.getLastName());
				ps.executeUpdate();
				ps.close();
	 
			} catch (SQLException e) {
				throw new RuntimeException(e);
	 
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {}
				}
			}
		}
		
		public List<User> list(){			 
			String sql = "SELECT * FROM USER";
	 
			Connection conn = null;
	 
			try {
				conn = dataSource.getConnection();
				PreparedStatement ps = conn.prepareStatement(sql);
				List<User> users = new ArrayList<User>();
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					User user = new User();
					user.setId(rs.getInt("id"));
					user.setFirstName(rs.getString("first_name"));
					user.setLastName(rs.getString("last_name"));
					users.add(user);
				}
				rs.close();
				ps.close();
				return users;
			} catch (SQLException e) {
				throw new RuntimeException(e);
			} finally {
				if (conn != null) {
					try {
					conn.close();
					} catch (SQLException e) {}
				}
			}
		}
}