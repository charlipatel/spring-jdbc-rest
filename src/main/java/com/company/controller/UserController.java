package com.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.company.dao.JdbcUserDAO;
import com.company.model.User;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired 
	private JdbcUserDAO userDAO;	
	
	@RequestMapping(value="/view", method = RequestMethod.GET)
	public String view(ModelMap model) {
		return "list";
	}

	@RequestMapping(value="/list", method = RequestMethod.GET)
	public @ResponseBody List<User> list(ModelMap model) {
		List<User> users = userDAO.list();	
		return users;
	}
	
	@RequestMapping(value="/insert", method = RequestMethod.POST)
	public @ResponseBody User insert(@ModelAttribute User user, ModelMap model) {
		userDAO.insert(user);
		return user;
	}
}