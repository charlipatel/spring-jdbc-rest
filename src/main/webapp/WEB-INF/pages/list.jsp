<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<style type="text/css">
<!--
.style1 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
<body>
	<h1 align="center">Add User</h1>	
	<table align="center">
	<form id="userForm" action="/user/insert" method="POST">
	<tr>
	<td>First Name:</td>
	<td><input name="firstName" id="firstName"/></td>
	</tr>
	<tr>
	<td>Last Name:</td>
	<td><input name="lastName" id="lastName"/></td>
	</tr>
		<tr>
		<td height="58" colspan="2"><div align="center">
		  <input type="submit" value="Submit"/>
		  </div></td>
		</tr>
	</form>	
	
	<tr>
	<td colspan="2"> 
	  <div align="center">
	    <input type="button" value="Retrive Users" id="retriveUsers"  align="absmiddle"/> 
      </div></td>
	  </tr>
	</table>
	<table id="userList" align="center">
		<thead>
      <th><div align="center"><span class="style1">No</span></div></th>
	      	<th><div align="center"><span class="style1">First Name</span></div></th>
	      	<th><div align="center"><span class="style1">Last Name</span></div></th>
		    <div align="center"><span class="style1">
         </thead>
	          </span></div>
	    <tbody>
	    </tbody>		      	
    </table>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>
    	$(function(){
    		//callback handler for form submit
    		$("#userForm").submit(function(e){
    		    var firstName = $("#firstName").val();
    		    var lastName = $("#lastName").val();
    		    var formURL = $(this).attr("action");
    		    
    		    var data = {
		        	firstName: firstName,
		        	lastName: lastName
		        };
    		    
    		    $.post(formURL, data, function(response) {
    		        // Do something with the request
    		    }, 'application/json');
    		    
    		    e.preventDefault(); //STOP default action    		    
    		});
    		
    		
    		
    		//list
    		$("#retriveUsers").click(function(e){
    			 $.getJSON("/user/list", function(data) {
    				var content ="";
                 	jQuery.each(data, function(i, val) {
                 		 content += "<tr><td>"+val.id + "</td><td>"+val.firstName+"</td><td>"+val.lastName +"</td></tr>";
                 	});
                 	$("#userList tbody").empty();
                 	$("#userList tbody").append(content);
                 });
    		});
    	});
    </script>
</body>
</html>